const IS_DEV = process.env.NODE_ENV !== "development" ? false : false; // true : backup, false: production

export const API_URL_BASE = IS_DEV
  ? "http://backup-aube.pthwas.xyz:8200/api"
  : "http://host-collection.com:8200/api";

  export const ENCODE_SECRET_KEY = process.env?.ENCODE_SECRET_KEY || "QiLCJhbGciOiJSUzI";