export interface User {
  id?: number | null;
  name?: string;
  email?: string;
  password?: string;
  remember_me?: boolean
}