import { User } from "../types";
import request from "./axiosClient";

const URL_LOGIN = "/login";
const URL_LOGOUT = "/logout";

export function loginAPI(data: User) {
  return request({
    url: URL_LOGIN,
    method: "post",
    data: { ...data, role: 3 },
    noNeedToken: true,
  });
}

export function logoutAPI(token: string, params: {}) {
  return request({
    url: URL_LOGOUT,
    method: "get",
    params,
    token,
  });
}