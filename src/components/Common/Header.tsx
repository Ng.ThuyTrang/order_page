import logo from "../../assets/images/logo.png"
import { TbMapPin } from "react-icons/tb"
import { BsClock } from "react-icons/bs"
import { BiPhoneCall } from "react-icons/bi"

const HeaderComponent: React.FC = () => {
  return (
    <div className="">
      <div className="header d-flex justify-between center fixed py-2">
        <div className="d-flex center">
          <img className="logo pl-40" src={logo} alt="logo" />
          <span className="font-bold text-base ml-5">BROADWAYばびゅーん☆</span>
        </div>
        <nav>
          <ul className="nav-links d-flex center ">
            <li><a className="d-flex center" href="#"><TbMapPin size={20} />札幌市中央区南5条西6丁目第3エイトビル4F</a></li>
            <li><a href="#">|</a></li>
            <li><a className="d-flex center" href="#"><BsClock size={20} />20:00 〜 6:00 (LO 5:00)</a></li>
          </ul>
        </nav>
        <a className="cta" href="#"><button className="d-flex center"><BiPhoneCall className="mr-10" size={18} />011-213-0873</button></a>
      </div>
    </div>

  );
}

export default HeaderComponent;