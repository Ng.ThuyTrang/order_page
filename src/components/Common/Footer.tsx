import React from 'react';
import { FaInstagram } from 'react-icons/fa';
import { FiTwitter } from 'react-icons/fi';
import { FaAngleUp } from 'react-icons/fa';
import { useEffect, useState } from 'react';

const FooterComponent: React.FC = () => {
  return (
    <div className="footer-homepage">
      <div className="d-flex middle center sticky">
        <span>All Rights Reserved. 無断転載禁止</span>
      </div>
    </div>
  );
}

export default FooterComponent;