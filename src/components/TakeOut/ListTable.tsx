import React, { useState } from 'react';
import { Col, Row, Modal, Button, Input, TreeSelect } from 'antd';
import type { DefaultOptionType } from 'antd/es/select';
import { MdOutlineAdd } from 'react-icons/md';
import { AiOutlineMinus } from 'react-icons/ai';
import { BsBookmarkDash } from 'react-icons/bs';
import ModalOrderTable from "../TakeOut/ModalOrderTable"
import ConfirmOrder from "./ConfirmOrder"
interface Props {
    setPageTakeout: React.Dispatch<React.SetStateAction<boolean>>;
}

const ListTable: React.FC<Props> = ({ setPageTakeout }) => {
    const [count, setCount] = useState<number>(0);
    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
    const [pageConfirm, setPageConfirm] = useState<boolean>(false);
    const showModal = () => {
        setIsModalOpen(true);
    };

    const rowTable = [
        {
            arrCol: [
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                    text: '(Tax Incl.)',
                },
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                    text: '(Tax Incl.)',
                },
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                    text: '(Tax Incl.)',
                },
            ],
        },
        {
            arrCol: [
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                },
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                },
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                },
            ],
        },
        {
            arrCol: [
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                },
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                },
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                },
            ],
        },
        {
            arrCol: [
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                },
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                },
                {
                    id: 1,
                    title: 'オリジナルチャンパン ホワイト',
                    price: '￥200,000',
                    add: 'オーダーに入れる',
                },
            ],
        },
    ];

    const [value, setValue] = useState<string>();
    const [treeData, setTreeData] = useState<Omit<DefaultOptionType, 'label'>[]>([
        { id: 1, pId: 0, value: '1', title: 'Champagne' },
        { id: 2, pId: 0, value: '2', title: 'Whisky Brandy' },
        { id: 3, pId: 0, value: '3', title: 'Shochu' },
    ]);

    const onChange = (newValue: string) => {
        console.log(newValue);
        setValue(newValue);
    };

    const renderRow = () => {
        return rowTable.map((x, index) => {
            return (
                <Row>
                    {x.arrCol.map((arr1, index) => {
                        return (
                            <Col span={8}>
                                <div className="mr-4 flex flex-col h-[170px] justify-between mb-4 bg-[#1F2A37] pr-4 rounded-sm p-[10px]">
                                    <div className="d-flex">
                                        <span className="text-[#fff] text-[15px] w-[300px]">{arr1.title}</span>
                                        <strong className="text-[#FAC515] text-[18px] font-medium">{arr1.price}</strong>
                                    </div>
                                    <div className="d-flex justify-end mt-[-20px]">
                                        <span className="text-[#DDC298] text-[15px]">(Tax Incl.)</span>
                                    </div>
                                    <div className="gr-count d-flex flex-end center">
                                        <Button
                                            onClick={() => setCount(count - 1)}
                                            className="minus w-[2.5rem] h-[2.5rem] rounded-full border-none"
                                        >
                                            <span className="">
                                                <AiOutlineMinus size={22} className="text-[#F3F4F6] " />
                                            </span>
                                        </Button>
                                        <span className="mr-[10px] ml-[10px] text-[#fff]">{count}</span>
                                        <Button
                                            onClick={() => setCount(count + 1)}
                                            className="add w-[2.5rem] h-[2.5rem] rounded-full bg-[#FAC515] border-none"
                                        >
                                            <span className="">
                                                <MdOutlineAdd size={22} />
                                            </span>
                                        </Button>
                                    </div>
                                    <div className="">
                                        <Button className=" w-[100%] rounded bg-[#FAC515] text-black border-none font-medium">
                                            {arr1.add}
                                        </Button>
                                    </div>
                                </div>
                            </Col>
                        );
                    })}
                </Row>
            );
        });
    };

    return (
        <>
            {!pageConfirm ? (
                <>
                    <div>
                        <Button onClick={() => setPageTakeout(false)} className="text-white border-none">
                            back
                        </Button>
                    </div>
                    <div className="listpage pt-[100px] w-[100%] d-flex pr-[50px] pl-[100px] m-0 h-[120vh] bg-[#111927]">
                        <div className="w-[336px]">
                            <div className="mb-[30px]"> 
                                <Input className="h-[35px] bg-[#384250] rounded  border-[#6C737F]" placeholder="検索" />
                            </div>
                            <div className="input-info mb-[110px] ">
                                <TreeSelect
                                    treeDataSimpleMode
                                    style={{ width: '100%' }}
                                    value={value}
                                    dropdownStyle={{ maxHeight: 400, overflow: 'auto', }}
                                    placeholder="DRINK MENU"
                                    onChange={onChange}
                                    treeData={treeData}
                                />
                            </div>
                            <div>
                                <TreeSelect className='input-info' style={{ width: '100%' }} placeholder="FOOD MENU" />
                            </div>
                        </div>
                        <div className="h-screen mx-8">
                            <div className="btn-order d-flex justify-end mb-[30px]">
                                <Button onClick={showModal} className="d-flex middle center  w-[150px] mr-[15px] text-[16px]">
                                    <BsBookmarkDash size={18} className="mr-[5px]" />
                                    order ({count})
                                </Button>
                            </div>
                            <ModalOrderTable
                                isModalOpen={isModalOpen}
                                setIsModalOpen={setIsModalOpen}
                                count={count}
                                setCount={setCount}
                                pageConfirm={pageConfirm}
                                setPageConfirm={setPageConfirm}

                            />
                            <div>{renderRow()}</div>
                        </div>
                    </div>
                </>

            ) : (
                <>
                    <ConfirmOrder
                        setPageConfirm={setPageConfirm}
                    />
                </>
            )}

        </>
    );
};

export default ListTable;
