import React, { useState } from 'react';
import TableBooking from '../TableBooking/TableBooking';
import { Button, Checkbox } from 'antd';
import * as cts from '../../locales/i18n';
import  ListTable  from "../TakeOut/ListTable"
import { RiHome3Line } from "react-icons/ri"
import { IoIosArrowForward } from "react-icons/io"


interface Props {
  setCurrentTab: React.Dispatch<React.SetStateAction<number>>;
}
const TakeOut: React.FC<Props> = ({ setCurrentTab }) => {

  const [pageTakeout, setPageTakeout] = useState<boolean>(false);
  return (
    <>
      <div className="tablebooking-wapper pt-[90px] h-screen">
        {!pageTakeout ? (
          <>
            <Button onClick={() => setCurrentTab(0)} className="text-white mt-[10px] ml-[40px] border-none bg-[#111927] hover:bg-[#111927] focus:text-[#fff] focus:border-[#fff]">
            <span><RiHome3Line size={26} /></span>
              <span>
              <IoIosArrowForward size={26}/>
              </span>
            </Button>
            <div className="content mb-[50px]">
              <span className="title text-[30px] text-[#DDC298] d-flex middle center mb-[40px]">
                ご予約にあたってのお願い
              </span>
              <p className="leading-8">
                ※予約不可のお日にちでも、店舗に直接お問い合わせいただければお席がご用意できることがありま
                す。 ※ご予定の人数が選べない場合は直接店舗までお問い合わせをお願いいたします。
                ※当日のご予約はお電話にて承っております。 ※ご予約
                の変更またはキャンセルについては、上記番号までお電話ください。
                ※ご連絡なしでご予約時間を15分過ぎた際には、お待ちいただいているお客様を優先的にご案内する場合がございます。
                ※ご予約の際にいただいた電話番号やメールアドレスに不備がある場合は、ご予約をキャンセル扱いとさせていただく場合がございます。必ず正しい電話番号、メールアドレスをご入力いただきますようお願いいたします。
                ※ご連絡がなくご来店いただけなかったお客様は、以後のご予約を承ることができない場合がございますので、あらかじめご了承ください。
              </p>
              <Checkbox
                className="text-[#fff] "
              // onChange={onChange}
              >
                上記の内容を確認しました
              </Checkbox>
            </div>
            <div className="btn-booking mt-20 d-flex middle center">
              <Button
                className="h-[35px] w-[400px] text-[15px] bg-[#fff] rounded"
                onClick={() => setPageTakeout(true)}
              >
                予約ページに進む
              </Button>
            </div>
          </>
        ) : (
          <>
            <div>
              <ListTable setPageTakeout={setPageTakeout}/>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default TakeOut;
