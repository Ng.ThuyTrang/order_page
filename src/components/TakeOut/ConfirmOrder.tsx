import React, { useState } from 'react';
import {
    Input,
    Form,
    Button,
    Breadcrumb,
    TreeSelect,
    Modal,
    DatePicker,
    Space,
    ConfigProvider,
    Select,
} from 'antd';
import { TbMapPin } from 'react-icons/tb';
import { BsClock } from 'react-icons/bs';
import { BiPhoneCall } from 'react-icons/bi';
import { FaRegUserCircle } from 'react-icons/fa';
import { IoPhonePortraitOutline } from 'react-icons/io5';
import { AiOutlineMail, AiOutlineCalendar, AiOutlineClockCircle } from 'react-icons/ai';
import { BsPerson } from 'react-icons/bs';
import { GiWoodenChair } from 'react-icons/gi';
import PageChange from "../TakeOut/PageChange"

interface Props {
    setPageConfirm: React.Dispatch<React.SetStateAction<boolean>>;

}

const ConfirmOrder: React.FC<Props> = ({ setPageConfirm }) => {
    const { TextArea } = Input;
    const [countText, setCounttext] = useState<number>(0);
    const [valueText, setValueText] = useState<string>('');
    const [page, setPage] = useState<boolean>(false);

    return (
        <>
            {
                !page ? (
                    <>
                        <div className='ml-[30px]'>
                            <Button onClick={() => setPageConfirm(false)} className='text-[#fff]'>back</Button>
                        </div>
                        <div className='d-flex justify-center text-[#DDC298] text-[30px] pb-[50px] font-semibold'>
                            予約情報
                        </div>
                        <div className='cofirm-wapper d-flex pr-[200px] pl-[200px]'>
                            <div className='col-1 d-flex flex-1 text-[#fff]'>
                                <span className="text-confirm text-[#fff] text-[20px] ">
                                    予約者情報 <span className=" text-[#DDC298] text-[15px]">(必須）</span>
                                    <div className='border-col1'></div>
                                </span>
                                <div className='form-confirm mt-[40px]'>
                                    <form className=''>
                                        <div className='form-text d-flex items-baseline'>
                                            <div className='mr-[10px]'>
                                                <FaRegUserCircle className='text-[#ddc298]' size={22} />
                                            </div>
                                            <div className="user-box">
                                                <input type="text" name="" required />
                                                <label>お名前</label>
                                            </div>
                                        </div>
                                        <div className='h-[0.3px] bg-[#fff] w-[32vw] mt-[-10px] mb-[25px]'></div>

                                        <div className='form-text d-flex items-baseline'>
                                            <div className='mr-[10px]'>
                                                <FaRegUserCircle className='text-[#ddc298]' size={22} />
                                            </div>
                                            <div className="user-box">
                                                <input type="text" name="" required />
                                                <label>お名前（かな）</label>
                                            </div>
                                        </div>
                                        <div className='h-[0.3px] bg-[#fff] w-[32vw] mt-[-10px] mb-[25px]'></div>

                                        <div className='form-text d-flex items-baseline'>
                                            <div className='mr-[10px]'>
                                                <IoPhonePortraitOutline className='text-[#ddc298]' size={22} />
                                            </div>
                                            <div className="user-box">
                                                <input type="text" name="" required />
                                                <label>電話番号</label>
                                            </div>
                                        </div>
                                        <div className='h-[0.3px] bg-[#fff] w-[32vw] mt-[-10px] mb-[25px]'></div>

                                        <div className='form-text d-flex items-baseline'>
                                            <div className='mr-[10px]'>
                                                <AiOutlineMail className='text-[#ddc298]' size={22} />
                                            </div>
                                            <div className="user-box">
                                                <input type="text" name="" required />
                                                <label>メールアドレス</label>
                                            </div>
                                        </div>
                                        <div className='h-[0.3px] bg-[#fff] w-[32vw] mt-[-10px] mb-[25px]'></div>
                                    </form>
                                    <div className="form-confirm  mt-[10px]">
                                        <span className=" text-[#fff] text-[20px]">
                                            予約 <span className=" text-[#DDC298] text-[15px]">(必須）</span>
                                        </span>
                                        <div className="h-[0.3px] bg-[#DDC298] w-[32vw]"></div>
                                        <form className=''>
                                            <div className='form-text d-flex items-baseline mt-[20px]'>
                                                <div className='mr-[10px]'>
                                                    <AiOutlineCalendar className='text-[#ddc298]' size={22} />
                                                </div>
                                                <div className="user-box">
                                                    <input type="text" name="" required />
                                                    <label>来店日</label>
                                                </div>
                                            </div>
                                            <div className='h-[0.3px] bg-[#fff] w-[32vw] mt-[-10px] mb-[20px]'></div>

                                            <div className='form-text d-flex items-baseline mt-[-5px]'>
                                                <div className='mr-[10px]'>
                                                    <AiOutlineClockCircle className='text-[#ddc298]' size={22} />
                                                </div>
                                                <div className="user-box">
                                                    <input type="text" name="" required />
                                                    <label>来店時</label>
                                                </div>
                                            </div>
                                            <div className='h-[0.3px] bg-[#fff] w-[32vw] mt-[-5px] mb-[20px]'></div>
                                        </form>
                                        <div className="text-mess w-[100%] mt-[30px] mb-[10px]">
                                            <span className="mt-0 text-[#fff]">ご要望とご質問</span>
                                            <Form.Item className="mess">
                                                <TextArea
                                                    maxLength={200}
                                                    value={valueText}
                                                    onChange={(e) => {
                                                        setValueText(e.target.value);
                                                        setCounttext(e.target.value.length);
                                                    }}
                                                    className="text-[#fff]  mt-[10px] bg-[#111927]"
                                                    rows={2}
                                                />
                                            </Form.Item>

                                        </div>
                                        <div className="condition d-flex text-[#fff] text-[13px]">
                                            <span className="d-flex flex-1 text-xs mr-[20px]">
                                                ご予約についてのご要望やご質問はこちらに記入をお願いいたします。（例）アレルギー、苦手な食材、バースデープレートのお祝いメッセージなど
                                            </span>
                                            <span className="count d-flex flex-10">{countText} / 200</span>
                                        </div>
                                        <div className="btn-booking mt-[98px] d-flex middle center">
                                            <Button onClick={() => { }} className="btn-active d-flex middle center ">
                                                予約ページに進む
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='col-2 d-flex flex-1 text-[#fff] flex-col pl-[24px]'>
                                <div className='gr'>
                                    <span className='text-[#fff] text-[20px]'>オーダー</span>
                                    <div className='border-col1'></div>
                                </div>
                                <div className="d-flex mb-10 mt-10 w-[100%]">
                                    <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">オリジナルチャンパン ホワイト</span>
                                    <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>
                                </div>
                                <div className='mt-[-5px]'>
                                    <span>数量: 1</span>
                                </div>
                                <div className='border-ctn mt-[20px] '></div>
                                <div className="d-flex mb-10 mt-10 w-[100%]">
                                    <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">オリジナルチャンパン ホワイト</span>
                                    <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>
                                </div>
                                <div className='mt-[-5px]'>
                                    <span>数量: 1</span>
                                </div>
                                <div className='border-ctn mt-[20px] '></div>
                                <div className="d-flex mb-10 mt-10 w-[100%]">
                                    <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">オリジナルチャンパン ホワイト</span>
                                    <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>
                                </div>
                                <div className='mt-[-5px]'>
                                    <span>数量: 1</span>
                                </div>
                                <div className='border-ctn mt-[20px] '></div>
                                <div className="d-flex mb-10 mt-10 w-[100%]">
                                    <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">オリジナルチャンパン ホワイト</span>
                                    <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>
                                </div>
                                <div className='mt-[-5px]'>
                                    <span>数量: 1</span>
                                </div>
                                <div className='border-ctn mt-[20px]'></div>
                                <div className='form-confirm mb-[40px] mt-[20px] d-flex'>
                                    <form className='d-flex flex-1'>
                                        <div className="user-box">
                                            <input type="text" name="" required />
                                            <label>クーポン</label>
                                        </div>
                                    </form>
                                    <div>
                                        <Button className='bg-[#FAC515] rounded-md h-[40px]'>利用</Button>
                                    </div>
                                    <div className='bor-price mt-[20px]'></div>

                                </div>
                                <div className='border-ctn'></div>
                                <div className="d-flex mb-10 mt-5 w-[100%]">
                                    <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">小計</span>
                                    <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>

                                </div>
                                <div className="d-flex mb-10 mt-5 w-[100%]">
                                    <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">税金</span>
                                    <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>

                                </div>
                                <div className="d-flex mb-10 mt-5 w-[100%]">
                                    <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">割引き (COUPON50)</span>
                                    <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>

                                </div>
                                <div className='border-ctn'></div>
                                <div className="d-flex mb-10 mt-5 w-[100%]">
                                    <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">合計</span>
                                    <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>

                                </div>
                                <div className="btn-booking mt-[29px] d-flex middle center">
                                    <Button onClick={() => setPage(true)} className="btn-active d-flex middle center ">
                                        確認ページへ
                                    </Button>
                                </div>
                            </div>

                        </div>
                    </>) : (<>
                        <PageChange
                            setPage={setPage}
                        />
                    </>)
            }

        </>

    )
}

export default ConfirmOrder;
