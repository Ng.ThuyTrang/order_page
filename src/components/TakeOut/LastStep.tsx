import React, { useState } from 'react'
import calendar from "../../assets/images/Calendar.png"
import { Button } from "antd"

interface Props {
    setLastPage: React.Dispatch<React.SetStateAction<boolean>>;
}

const LastStep: React.FC<Props> = ({ setLastPage}) => {

    return (
        <>
            <Button onClick={() => setLastPage(false)} className='text-white border-none'>
                back
            </Button>
            <div className="tablebooking-wapper-step pt-[40px]">
                <>
                    <div className="content-step  pr-[300px] pl-[300px] ">
                        <span className="title text-[30px] text-[#DDC298] d-flex middle center mb-[30px]">ご予約が確定いたしました</span>
                        <div className="step-end">
                            <span className='d-flex middle center font-bold text-[18px] mb-10'>
                                ご予約番号
                            </span>
                            <span className='d-flex middle center mb-10 font-bold text-[25px]'>#00001</span>
                            <span className='d-flex middle center text-center text-[15px]'>ご予約番号は必ずお控え下さい。メールが届かない場合は、予約
                                <br />
                                番号にてお店にお問い合わせ下さい。</span>
                            <br />
                            <div className='btn-sub-img d-flex middle center'>
                                <Button className='h-[35px] w-[300px] text-[13px] rounded'>
                                    <img className='mr-[10px]' src={calendar} alt="" />
                                    この予約をGoogleカレンダーへ追加
                                </Button>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                    <div className='submit-btn d-flex middle center '>
                        <Button className='mt-[30px] w-[60%] h-[35px] rounded'>
                            店舗トップへ
                        </Button>
                    </div>
                </>

            </div>

        </>
    )
}

export default LastStep;