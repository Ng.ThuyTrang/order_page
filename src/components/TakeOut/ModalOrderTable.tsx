import React, { useState } from 'react';
import { Col, Row, Modal, Button, Input, TreeSelect } from 'antd';
import type { TreeSelectProps } from 'antd';
import type { DefaultOptionType } from 'antd/es/select';
import { MdOutlineAdd } from 'react-icons/md';
import { AiOutlineMinus } from 'react-icons/ai';
import { BsBookmarkDash, BsFillTrashFill } from 'react-icons/bs';
import { IoIosArrowBack } from 'react-icons/io';


interface Props {
  setIsModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
  isModalOpen: boolean;
  setCount: React.Dispatch<React.SetStateAction<number>>;
  count: number;
  pageConfirm: boolean;
  setPageConfirm: React.Dispatch<React.SetStateAction<boolean>>;


}

const ModalOrderTable: React.FC<Props> = ({ isModalOpen, setIsModalOpen, count, setCount, pageConfirm, setPageConfirm }) => {



  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
   
      <div className="modal-order">
        <Modal
          className="modal-table bg-[#111927]"
          bodyStyle={{ overflowY: 'auto', maxHeight: 'calc(100vh - 200px)' }}
          title="オーダー "
          visible={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={null}
        >
          <div className="h-auto mb-[200px] p-[24px]">
            <div className="d-flex mb-10">
              <span className="d-flex flex-1 text-[#F3F4F6] ">オリジナルチャンパン ホワイト</span>
              <span className="text-[#F3F4F6] ">￥200,000</span>
            </div>
            <div className="d-flex mb-10">
              <div className="gr-count-modal d-flex flex-end center flex-1 mb-[5px]">
                <Button
                  style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                  onClick={() => setCount((pre) => pre + 1)}
                  className="minus-modal d-flex justify-center center w-[2rem] h-[2rem] rounded-full border-none bg-[#4D5761]"
                >
                  <span className="">
                    <AiOutlineMinus size={15} className="text-[#F3F4F6] " />
                  </span>
                </Button>
                <span className="mr-[10px] ml-[10px] text-[#fff]">{count}</span>
                <Button
                  style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                  onClick={() => setCount((pre) => pre + 1)}
                  className="add-modal w-[2rem] h-[2rem] rounded-full bg-[#FAC515] border-none"
                >
                  <span className="">
                    <MdOutlineAdd size={15} />
                  </span>
                </Button>
              </div>
              <div>
                <BsFillTrashFill color='#fff' size={22} />
              </div>
            </div>
            <div className="border-modal"></div>
            <div className="d-flex mb-10 mt-[15px]">
              <span className="d-flex flex-1 text-[#F3F4F6] ">オリジナルチャンパン ホワイト</span>
              <span className="text-[#F3F4F6] ">￥200,000</span>
            </div>
            <div className="d-flex mb-10">
              <div className="gr-count-modal d-flex flex-end center flex-1 mb-[5px]">
                <Button
                  style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                  onClick={() => setCount((pre) => pre + 1)}
                  className="minus-modal w-[2rem] h-[2rem] rounded-full border-none bg-[#4D5761]"
                >
                  <span className="">
                    <AiOutlineMinus size={15} className="text-[#F3F4F6] " />
                  </span>
                </Button>
                <span className="mr-[10px] ml-[10px] text-[#fff]">{count}</span>
                <Button
                  style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                  onClick={() => setCount((pre) => pre + 1)}
                  className="add-modal w-[2rem] h-[2rem] rounded-full bg-[#FAC515] border-none"
                >
                  <span className="">
                    <MdOutlineAdd size={15} />
                  </span>
                </Button>
              </div>
              <div>
                <BsFillTrashFill color='#fff' size={22} />
              </div>
            </div>
            <div className="border-modal"></div>
            <div className="d-flex mb-10  mt-[15px]">
              <span className="d-flex flex-1 text-[#F3F4F6] ">オリジナルチャンパン ホワイト</span>
              <span className="text-[#F3F4F6] ">￥200,000</span>
            </div>
            <div className="d-flex mb-10">
              <div className="gr-count-modal d-flex flex-end center flex-1 mb-[5px]">
                <Button
                  style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                  onClick={() => setCount((pre) => pre + 1)}
                  className="minus-modal w-[2rem] h-[2rem] rounded-full border-none bg-[#4D5761]"
                >
                  <span className="">
                    <AiOutlineMinus size={15} className="text-[#F3F4F6] " />
                  </span>
                </Button>
                <span className="mr-[10px] ml-[10px] text-[#fff]">{count}</span>
                <Button
                  style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                  onClick={() => setCount((pre) => pre + 1)}
                  className="add-modal w-[2rem] h-[2rem] rounded-full bg-[#FAC515] border-none"
                >
                  <span className="">
                    <MdOutlineAdd size={15} />
                  </span>
                </Button>
              </div>
              <div>
                <BsFillTrashFill color='#fff' size={22} />
              </div>
            </div>
            <div className="border-modal"></div>
            <div className="d-flex mb-10  mt-[15px]">
              <span className="d-flex flex-1 text-[#F3F4F6] ">オリジナルチャンパン ホワイト</span>
              <span className="text-[#F3F4F6] ">￥200,000</span>
            </div>
            <div className="d-flex mb-10">
              <div className="gr-count-modal d-flex flex-end center flex-1 mb-[5px]">
                <Button
                  style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                  onClick={() => setCount((pre) => pre + 1)}
                  className="minus-modal w-[2rem] h-[2rem] rounded-full border-none bg-[#4D5761]"
                >
                  <span className="">
                    <AiOutlineMinus size={15} className="text-[#F3F4F6] " />
                  </span>
                </Button>
                <span className="mr-[10px] ml-[10px] text-[#fff]">{count}</span>
                <Button
                  style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                  onClick={() => setCount((pre) => pre + 1)}
                  className="add-modal w-[2rem] h-[2rem] rounded-full bg-[#FAC515] border-none"
                >
                  <span className="">
                    <MdOutlineAdd size={15} />
                  </span>
                </Button>
              </div>
              <div>
                <BsFillTrashFill color='#fff' size={22} />
              </div>
            </div>
            <div className="border-modal"></div>
          </div>
          <div className="border-modal"></div>
          <div className="d-flex mb-10 mt-5 pr-[24px] pl-[24px]">
            <span className="d-flex flex-1 text-[#F3F4F6] ">お金</span>
            <span className="text-[#F3F4F6] ">￥800,000</span>
          </div>
          <div className="d-flex mb-10 pr-[24px] pl-[24px]">
            <span className="d-flex flex-1 text-[#F3F4F6] ">お金</span>
            <span className="text-[#F3F4F6] ">￥800,000</span>
          </div>
          <div className="border-modal"></div>
          <div className="d-flex mb-10 mt-5 pr-[24px] pl-[24px]">
            <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">合計金額</span>
            <span className="text-[#F3F4F6] text-[20px]">￥800,000</span>
          </div>
          <div className='pr-[24px] pl-[24px]'>
            <Button onClick={() => setPageConfirm(true)} className='w-full h-[35px] bg-[#FAC515] rounded'>xác nhận order</Button>
          </div>

        </Modal>

      </div>

    </>
  );
};

export default ModalOrderTable;
