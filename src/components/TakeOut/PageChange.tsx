import React, { useState } from 'react';
import {
    Button,
} from 'antd';

import LastStep from "./LastStep"

interface Props {
    setPage: React.Dispatch<React.SetStateAction<boolean>>;
}
const PageChange: React.FC<Props> = ({ setPage }) => {

    const [lastPage, setLastPage] = useState<boolean>(false);

    return (
        <>
            {!lastPage ? (
                <>
                    <div className='ml-[30px]'>
                        <Button onClick={() => setPage(false)} className='text-[#fff]'>back</Button>
                    </div>
                    <div className='d-flex justify-center text-[#DDC298] text-[30px] pb-[50px] font-semibold'>
                        予約情報
                    </div>
                    <div className='cofirm-wapper d-flex pr-[200px] pl-[200px]'>
                        <div className='col-1 d-flex flex-1 text-[#fff]'>
                            <span className="text-confirm text-[#fff] text-[20px] ">
                                ご予約の確認
                                <div className='border-col1'></div>
                            </span>
                            <div className='form-confirm mt-[40px] w-[42vw]'>
                                <div className="d-flex pr-[250px] mb-[20px] mt-[10px]">
                                    <span className="d-flex flex-1">お名前</span>
                                    <span className="d-flex flex-1">田中一郎（たなかいちろう）</span>
                                </div>
                                <div className="d-flex pr-[250px] mb-[20px] ">
                                    <span className="d-flex flex-1">お名前</span>
                                    <span className="d-flex flex-1">0 - 0000 - 0000</span>
                                </div>
                                <div className="d-flex pr-[250px] mb-[20px] ">
                                    <span className="d-flex flex-1">メールアドレス</span>
                                    <span className="d-flex flex-1">id@gmail.com</span>
                                </div>
                                <div className="d-flex pr-[250px] mb-[20px] ">
                                    <span className="d-flex flex-1">来店日時</span>
                                    <span className="d-flex flex-1">2022/09/20（金）｜　21:00</span>
                                </div>
                                <div className="d-flex mb-[20px] w-[510px]">
                                    <span className="pr-[120px]">来店日時</span>
                                    <span className="d-flex flex-1 mr-[20px]">ご連絡がなくご来店いただけなかったお客様は、以後のご予約を承ることができない場合がございますので、あらかじめご了承ください。</span>
                                </div>
                                <div className=" mt-[350px]">
                                    <Button onClick={() => { }} className="h-[40px] w-[250px] d-flex middle center text-[#fff] rounded">
                                        確認ページへ
                                    </Button>
                                </div>
                            </div>
                        </div>
                        <div className='col-2 d-flex flex-1 text-[#fff] flex-col pl-[24px]'>
                            <div className='gr'>
                                <span className='text-[#fff] text-[20px]'>オーダー</span>
                                <div className='border-col1'></div>
                            </div>
                            <div className="d-flex mb-10 mt-10 w-[100%]">
                                <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">オリジナルチャンパン ホワイト</span>
                                <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>
                            </div>
                            <div className='mt-[-5px]'>
                                <span>数量: 1</span>
                            </div>
                            <div className='border-ctn mt-[20px] '></div>
                            <div className="d-flex mb-10 mt-10 w-[100%]">
                                <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">オリジナルチャンパン ホワイト</span>
                                <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>
                            </div>
                            <div className='mt-[-5px]'>
                                <span>数量: 1</span>
                            </div>
                            <div className='border-ctn mt-[20px] '></div>
                            <div className="d-flex mb-10 mt-10 w-[100%]">
                                <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">オリジナルチャンパン ホワイト</span>
                                <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>
                            </div>
                            <div className='mt-[-5px]'>
                                <span>数量: 1</span>
                            </div>
                            <div className='border-ctn mt-[20px] '></div>
                            <div className="d-flex mb-10 mt-10 w-[100%]">
                                <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">オリジナルチャンパン ホワイト</span>
                                <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>
                            </div>
                            <div className='mt-[-5px]'>
                                <span>数量: 1</span>
                            </div>
                            <div className='border-ctn mt-[20px]'></div>
                            <div className="d-flex mb-10 mt-5 w-[100%]">
                                <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">小計</span>
                                <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>

                            </div>
                            <div className="d-flex mb-10 mt-5 w-[100%]">
                                <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">税金</span>
                                <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>

                            </div>
                            <div className="d-flex mb-10 mt-5 w-[100%]">
                                <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">割引き (COUPON50)</span>
                                <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>

                            </div>
                            <div className='border-ctn'></div>
                            <div className="d-flex mb-10 mt-5 w-[100%]">
                                <span className="d-flex flex-1 text-[#F3F4F6] text-[16px]">合計</span>
                                <span className="text-[#F3F4F6] text-[20px]">￥200,000</span>

                            </div>
                            <div className="btn-booking mt-[29px] d-flex middle center">
                                <Button onClick={() => setLastPage(true)} className="btn-active d-flex middle center ">
                                    確認ページへ
                                </Button>
                            </div>
                        </div>
                    </div>
                </>
            ) : (
                <>
                    <LastStep setLastPage={setLastPage}/>
                </>
            )}

        </>
    )
};
export default PageChange;