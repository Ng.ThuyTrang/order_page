import React, { useEffect, useState } from 'react'
import { HomeOutlined, UserOutlined } from '@ant-design/icons';
import { Breadcrumb } from "antd"; 
import { useDispatch } from 'react-redux';



export const SetupPage = () => {
  const dispatch = useDispatch();
  const [page, setPage] = useState<number>(0);
  const menu = [
    { id: 1, title: "NEW_RELEASE" },
    { id: 2, title: "FEATURED_PRODUCTS" },
    { id: 3, title: "CATEGORY1" },
    { id: 4, title: "RANKING" },
    { id: 5, title: "SEARCH_BY_KEYWORD1" },
    { id: 6, title: "BLOG" },
  ];
  const auth = [
    { title: "LOGIN", href: '/my-page' },
    { title: "REGISTER", href: '/' },
    { title: "CART", href: '/' },
    { title: "VOUCHERS", href: '/' },
  ];
  useEffect(() => {
    async function setSidebar() {
      const activeSidebar = await localStorage.getItem('PAGE_ACTIVE_MENU');
      if (activeSidebar) {
        setPage(parseInt(activeSidebar));
      } else {
        setPage(0);
        localStorage.setItem('PAGE_ACTIVE_MENU', '0');
      }
    }
    setSidebar();
  }, [page]);

  return (
    <>
      <div className="bg-[#DCBA78]/50 flex justify-around px-32 h-[77px] items-center">
        {menu.map((item) => (
          <div
            className="tab_nav"
            onClick={() => {
              setPage(item.id);
              localStorage.setItem('PAGE_ACTIVE_MENU', item.id.toString());
            }}
            key={item.id}>
            <span className="span_nav text-Deep-moss-green font-bold text-[24px] hover:text-Deep-moss-green cursor-pointer ">
              {item.title}
            </span>
          </div>
        ))}
      </div>
    </>
  )
}
