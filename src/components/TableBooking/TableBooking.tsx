import React, { useState, useRef } from 'react';
import { Button, Checkbox } from 'antd';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import { BsPerson } from 'react-icons/bs';
import { GiWoodenChair } from 'react-icons/gi';
import logo from "../../assets/images/logo.png"
import { TbMapPin } from "react-icons/tb"
import { BsClock } from "react-icons/bs"
import { BiPhoneCall } from "react-icons/bi"
import { Breadcrumb } from 'antd';
import { HomeOutlined, UserOutlined } from '@ant-design/icons';
import Step2 from "../TableBooking/Step2"
import * as cts from "../../locales/i18n"
import { RiHome3Line } from "react-icons/ri"
import { IoIosArrowForward } from "react-icons/io"
interface Props {
  setCurrentTab: React.Dispatch<React.SetStateAction<number>>;
}

const onChange = (e: CheckboxChangeEvent) => {
  console.log(`checked = ${e.target.checked}`);
};



const TableBooking: React.FC<Props> = ({ setCurrentTab }) => {

  const [checkbox, setCheckBox] = useState(false);

  const [isSubscribed, setIsSubscribed] = useState(false);

  const [page, setPage] = useState<boolean>(false);

  const handleChange = () => {
    setIsSubscribed(isSubscribed => !isSubscribed);
  };


  return (
    <>
      <div className='tablebooking-wapper pt-[90px] h-screen'>
        {!page ? (
          <>
            <Button onClick={() => setCurrentTab(0)} className='btn-back text-white mt-[10px] ml-[40px] border-none'>
              <span><RiHome3Line size={26} /></span>
              <span>
              <IoIosArrowForward size={26}/>
              </span>
            </Button>

            <div className='content mb-[50px]'>
              <span className='title text-[30px] text-[#DDC298] d-flex middle center mb-[40px]'>
                ご予約にあたってのお願い
              </span>
              <p className='leading-8'>
                ※予約不可のお日にちでも、店舗に直接お問い合わせいただければお席がご用意できることがありま
                す。
                ※ご予定の人数が選べない場合は直接店舗までお問い合わせをお願いいたします。
                ※当日のご予約はお電話にて承っております。
                ※ご予約 の変更またはキャンセルについては、上記番号までお電話ください。
                ※ご連絡なしでご予約時間を15分過ぎた際には、お待ちいただいているお客様を優先的にご案内する場合がございます。
                ※ご予約の際にいただいた電話番号やメールアドレスに不備がある場合は、ご予約をキャンセル扱いとさせていただく場合がございます。必ず正しい電話番号、メールアドレスをご入力いただきますようお願いいたします。
                ※ご連絡がなくご来店いただけなかったお客様は、以後のご予約を承ることができない場合がございますので、あらかじめご了承ください。
              </p >
              <Checkbox className='text-[#fff] ' onChange={onChange}>上記の内容を確認しました</Checkbox>
            </div>
            <div className='btn-booking mt-20 d-flex middle center'>
              <Button className="h-[35px] w-[400px] text-[15px] bg-[#fff] rounded"
                onClick={() => setPage(true)}>
                予約ページに進む
              </Button>
            </div>
          </>
        ) : (
          <>
            <div>
              <Step2 setPage={setPage} />
            </div>
          </>
        )}

      </div>
    </>

  )
};

export default TableBooking;

