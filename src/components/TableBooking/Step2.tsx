import React, { useState } from 'react';
import { FaRegUserCircle } from 'react-icons/fa';
import { IoPhonePortraitOutline } from 'react-icons/io5';
import { AiOutlineMail, AiOutlineCalendar, AiOutlineClockCircle } from 'react-icons/ai';
import { BsPerson } from 'react-icons/bs';
import { GiWoodenChair } from 'react-icons/gi';
import {
  Input,
  Form,
  Button,
  Breadcrumb,
  TreeSelect,
  Modal,
  DatePicker,
  Space,
  ConfigProvider,
  Select,
} from 'antd';
import logo from '../../assets/images/logo.png';
import { TbMapPin } from 'react-icons/tb';
import { BsClock } from 'react-icons/bs';
import { BiPhoneCall } from 'react-icons/bi';
import 'moment/locale/zh-cn';
import Step3 from '../TableBooking/Step3';
import { IoIosArrowBack } from "react-icons/io"


interface Props {
  setPage: React.Dispatch<React.SetStateAction<boolean>>;
}
const { TreeNode } = TreeSelect;

const treeData = [
  {
    title: 'Node1',
    value: '0-0',
    children: [
      {
        title: 'Child Node1',
        value: '0-0-1',
      },
      {
        title: 'Child Node2',
        value: '0-0-2',
      },
    ],
  },
  {
    title: 'Node2',
    value: '0-1',
  },
];

const Step2: React.FC<Props> = ({ setPage }) => {
  const { TextArea } = Input;
  const [countText, setCounttext] = useState<number>(0);
  const [valueText, setValueText] = useState<string>('');
  const [value, setValue] = useState<string | undefined>(undefined);

  const onChange = (newValue: string) => {
    console.log(newValue);
    setValue(newValue);
  };

  const [isModalOpen, setIsModalOpen] = useState(false);

  const { Option } = Select;

  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const [page2, setPage2] = useState<boolean>(false);

  const number = [
    { hour: '19:00' },
    { hour: '19:30' },
    { hour: '20:00' },
    { hour: '20:30' },
    { hour: '20:30' },
    { hour: '21:00' },
    { hour: '21:30' },
    { hour: '22:00' },
    { hour: '22:30' },
    { hour: '22:30' },
    { hour: '21:00' },
    { hour: '21:30' },
    { hour: '22:30' },
    { hour: '22:00' },
    { hour: '22:30' },
  ];

  const renderHours = () => {
    return number.map((item, index) => {
      return <Button className="btn-hour mr-10 mb-10">{item.hour}</Button>;
    });
  };


  return (
    <>
      <div className="tablebooking-wapper-step pr-[400px] pl-[400px] h-[122vh] bg-[#111927]">
        <>
          {!page2 ? (
            <>
              <Button onClick={() => setPage(false)} className="border-none text-white">
                back
              </Button>
              <div>
                <div className="content-step">
                  <span className="title text-[30px] text-[#DDC298] d-flex middle center mb-[30px]">テーブル予約</span>
                </div>
                <div className="info">
                  <span className=" text-[#fff] text-[20px] ">
                    予約者情報 <span className=" text-[#DDC298] text-[15px]">(必須）</span>
                  </span>
                </div>
                <div className="border-info mb-10"></div>
                <div className='login-box'>
                  <form className=''>
                    <div className='form-text d-flex items-baseline'>
                      <div className='mr-[10px]'>
                        <FaRegUserCircle className='text-[#ddc298]' size={22} />
                      </div>
                      <div className="user-box">
                        <input className='' type="text" name="" required />
                        <label>お名前</label>
                      </div>
                    </div>
                    <div className='border-icon'></div>
                    <div className='form-text d-flex items-baseline'>
                      <div className='mr-[10px]'>
                        <FaRegUserCircle className='text-[#ddc298]' size={22} />
                      </div>
                      <div className="user-box">
                        <input type="text" name="" required />
                        <label>お名前（かな）</label>
                      </div>
                    </div>
                    <div className='border-icon'></div>
                    <div className='form-text d-flex items-baseline'>
                      <div className='mr-[10px]'>
                        <IoPhonePortraitOutline className='text-[#ddc298]' size={22} />
                      </div>
                      <div className="user-box">
                        <input type="text" name="" required />
                        <label>電話番号</label>
                      </div>
                    </div>
                    <div className='border-icon'></div>
                    <div className='form-text d-flex items-baseline'>
                      <div className='mr-[10px]'>
                        <AiOutlineMail className='text-[#ddc298]' size={22} />
                      </div>
                      <div className="user-box">
                        <input type="text" name="" required />
                        <label>メールアドレス</label>
                      </div>
                    </div>
                    <div className='border-icon'></div>
                  </form>
                  <div className="info mt-[10px]">
                    <span className=" text-[#fff] text-[20px]">
                      予約 <span className=" text-[#DDC298] text-[15px]">(必須）</span>
                    </span>
                    <div className="border-info mb-10 w-[39vw]"></div>
                    <form className=''>
                      <div className='form-text d-flex items-baseline'>
                        <div className='mr-[10px]'>
                          <AiOutlineCalendar className='text-[#ddc298]' size={22} />
                        </div>
                        <div className="user-box">
                          <input type="text" name="" required />
                          <label>来店日</label>
                        </div>
                      </div>
                      <div className='border-icon'></div>
                      <div className='form-text d-flex items-baseline'>
                        <div className='mr-[10px]'>
                          <AiOutlineClockCircle className='text-[#ddc298]' size={22} />
                        </div>
                        <div className="user-box">
                          <input type="text" name="" required />
                          <label>来店時</label>
                        </div>
                      </div>
                      <div className='border-icon'></div>
                      <div className='form-text d-flex items-baseline'>
                        <div className='mr-[10px]'>
                          <BsPerson className='text-[#ddc298]' size={22} />
                        </div>
                        <div className="user-box">
                          <input type="text" name="" required />
                          <label>人数</label>
                        </div>
                      </div>
                      <div className='border-icon'></div>
                      <div className='form-text d-flex items-baseline'>
                        <div className='mr-[10px]'>
                          <GiWoodenChair className='text-[#ddc298]' size={22} />
                        </div>
                        <div className="user-box">
                          <input type="text" name="" required />
                          <label>電話番号</label>
                        </div>
                      </div>
                      <div className='border-icon'></div>
                    </form>
                    <div className="text-mess w-[40%] ">
                      <span className="mt-0 text-[#fff]">ご要望とご質問</span>
                      <Form.Item className="mess">
                        <TextArea
                          maxLength={200}
                          value={valueText}
                          onChange={(e) => {
                            setValueText(e.target.value);
                            setCounttext(e.target.value.length);
                          }}
                          className="text-[#fff]  mt-[10px] bg-[#111927]"
                          rows={2}
                        />
                      </Form.Item>

                    </div>
                    <div className="condition d-flex w-[43%] text-[#fff] text-[13px]">
                      <span className="d-flex flex-1 text-xs mr-[20px]">
                        ご予約についてのご要望やご質問はこちらに記入をお願いいたします。（例）アレルギー、苦手な食材、バースデープレートのお祝いメッセージなど
                      </span>
                      <span className="count d-flex flex-10">{countText} / 200</span>
                    </div>
                    <div className="btn-booking mt-20 d-flex middle center w-[43%] ">
                      <Button onClick={() => setPage2(true)} className="btn-active d-flex middle center ">
                        予約ページに進む
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ) : (
            <>
              <Step3 setPage2={setPage2} />
            </>
          )}
        </>
      </div>
    </>
  );
};

export default Step2;

