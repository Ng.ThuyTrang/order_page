import React, { useState } from 'react';
import logo from '../../assets/images/logo.png';
import { TbMapPin } from 'react-icons/tb';
import { BsClock } from 'react-icons/bs';
import { BiPhoneCall } from 'react-icons/bi';
import { Button } from 'antd';
import Step4 from "../TableBooking/Step4"
interface Props {
  setPage2: React.Dispatch<React.SetStateAction<boolean>>
}

const Step3: React.FC<Props> = ({ setPage2 }) => {
  const [page3, setPage3] = useState(false);

  return (
    <>
      <div className="tablebooking-wapper-step pt-[40px]">
        <>

        {!page3 ? (
        <>
          <Button onClick={() => setPage2(false)} className='text-white border-none'>back
          </Button>
          <div className="content-step">
            <span className="title text-[30px] text-[#DDC298] d-flex middle center mb-[30px]">ご予約の確認</span>
            <div className="step-end ">
              <div></div>
              <div className="tab-btn d-flex mb-15">
                <span className="d-flex flex-1">お名前</span>
                <span className="text-case d-flex">田中一郎（たなかいちろう）</span>
              </div>
              <div className="tab-btn d-flex mb-15">
                <span className="d-flex flex-1">お名前</span>
                <span className="text-case d-flex">0 - 0000 - 0000</span>
              </div>
              <div className="tab-btn d-flex mb-15">
                <span className="d-flex flex-1">メールアドレス</span>
                <span className="text-case d-flex">id@gmail.com</span>
              </div>
              <div className="tab-btn d-flex mb-15">
                <span className="d-flex flex-1">来店日時</span>
                <span className="text-case d-flex">2022/09/20（金）｜　21:00</span>
              </div>
              <div className="tab-btn d-flex mb-15">
                <span className="d-flex flex-1">人数</span>
                <span className="text-case d-flex">１名</span>
              </div>
              <div className="tab-btn d-flex mb-15">
                <span className="d-flex flex-1">席種選択</span>
                <span className="text-case d-flex">指定なし</span>
              </div>
              <div className="tab-btn d-flex mb-15">
                <span className="d-flex flex-1">ご要望とご質問</span>
                <span className="text-case d-flex">
                  ご連絡がなくご来店いただけなかったお客様は、以後のご予約を承ることができない場合がございますので、あらかじめご了承ください。
                </span>
              </div>
            </div>
            <div className='gr-btn d-flex mt-20'>
              <div className='confirm d-flex flex-1'>
                <Button className="h-[35px] w-[150px] text-[12px]">
                  確認する
                </Button>
              </div>
              <div className=''>
                <Button onClick={() => setPage3(true)} className="btn-change h-[35px] w-[300px] text-[12px]">
                  予約内容を変更する
                </Button>
              </div>
            </div>
          </div>
        </>) : (
          <>
            <Step4 setPage3={setPage3} />
          </>
        )}
        </>
      

      </div>
    </>
  );
};

export default Step3;
