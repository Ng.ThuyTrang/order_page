import Header from '../../components/Common/Header';
import Footer from '../../components/Common/Footer'
import { RouteProps, Route } from 'react-router-dom';
import { useState } from 'react';

// interface RouteProps{
//   headerPage: boolean
// }
export function MasterRouter(props: RouteProps) {

  return (
    <>
      <div className="wrapper">
        <Header />
        <div className={`article article-full`}>
          <div className="setup-page">
            <div className={`right-side`}>
              <div className="content">
                <Route {...props} />

              </div>
            </div>
          </div>
        </div>
        {/* <Footer /> */}
      </div>
    </>
  );
};

