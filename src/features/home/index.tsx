import React, { useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import { Carousel } from 'antd';
import background from "../../assets/images/background.png"
import item3 from "../../assets/images/item3.png"
import item1 from "../../assets/images/item1.png"
import item2 from "../../assets/images/item2.png"
import { AiOutlineArrowRight } from "react-icons/ai"
import { FaInstagram } from 'react-icons/fa';
import { FiTwitter } from 'react-icons/fi';
import { FaAngleUp } from 'react-icons/fa';
import TableBooking from "../../components/TableBooking/TableBooking";
import TakeOut from 'components/TakeOut/TakeOut';
import { HomeOutlined, UserOutlined } from '@ant-design/icons';
import { Breadcrumb, Alert } from 'antd';
import Step2 from 'components/TableBooking/Step2';
import logo from "../../assets/images/logo.png"
import { TbMapPin } from "react-icons/tb"
import { BsClock } from "react-icons/bs"
import { BiPhoneCall } from "react-icons/bi"
import HeaderComponent from 'components/Common/Header';
import Step3 from 'components/TableBooking/Step3';
import Step4 from 'components/TableBooking/Step4';
import  ListTable  from 'components/TakeOut/ListTable';
import {
  HashRouter as Router,
  Route,
  Switch,
  Link,
  withRouter
} from "react-router-dom";

const contentStyle: React.CSSProperties = {
  height: '500px',
  lineHeight: '500px',
  width: '100%'
};

const HomePage: React.FC = () => {

  const [showTopBtn, setShowTopBtn] = useState<boolean>(false);
  const [currentTab, setCurrentTab] = useState(0);



  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY > 400) {
        setShowTopBtn(true);
      } else {
        setShowTopBtn(false);
      }
    });
  }, []);

const goToTop = () => {
  window.scrollTo({
    top: 0,
    behavior: 'smooth',
  });
};


const tabs = [
  {
    title: "テーブル予約",
    more: "テーブル予約はこちらから"
  },
  {
    title: "テイクアウト",
    more: "テーブル予約はこちらから"
  },
];

return (

  <div className='homepage-wapper'>
    {currentTab === 0 && (<>
      <HeaderComponent />
      <Carousel autoplay>
        <div >
          <img style={contentStyle} src={background} />
        </div>
        <div >
          <img style={contentStyle} src={background} />
        </div>
        <div>
          <img style={contentStyle} src={background} />
        </div>
      </Carousel>
      <div className='content mb-5'>
        <div className="title-page d-flex center middle p-14 m-0">
          <span className='text-[36px] text-[#DDC298] font-bold '>
            ブッキング
            <div className="border-title"></div>
          </span>
        </div>
        <div className='d-flex middle center p-36 pb-10 pt-2'>

          <div className='booking relative'>
            <img className='item' src={item2} alt="wine.png" />
            <div className='text-wine absolute text-center'>
              <span className='text-2xl font-semibold'>テーブル予約</span>
              <br />
              <a onClick={() => setCurrentTab(2)} className='text-sm d-flex center list-none'>テーブル予約はこちらから<AiOutlineArrowRight className='ml-10' /> </a>
              <div className='border-food'></div>
            </div>
          </div>
          <div className='booking relative'>
            <img className='item' src={item1} alt="food.png" />
            <div className='text-wine absolute text-center'>
              <span className='text-2xl font-semibold'>テイクアウト</span>
              <br />
              <a onClick={() => setCurrentTab(1)} className='text-sm d-flex center list-none'>テイクアウトはこちらから<AiOutlineArrowRight className='ml-10' /> </a>
              <div className='border-food'></div>
            </div>
          </div>
        </div>
        <div>
          <div className="title-page d-flex center middle p-14 m-0">
            <span className='text-[36px] text-[#DDC298] font-bold '>
              店舗情報
              <div className="border-title"></div>
            </span>
          </div>
          <div className='d-flex w-full'>
            <div className='infomation w-3/5'>
              <div className='d-flex mb-15'>
                <span className='d-flex flex-1'>紹介</span>
                <span className='text-info d-flex'>シーシャ、ダーツ、カラオケ、フードがあり、男女スタッフ、黒服がいるお店です</span>
              </div>
              <div className='boder-info mb-15'></div>
              <div className='d-flex mb-10'>
                <span className='d-flex flex-1'>住所</span>
                <span className='text-info d-flex leading-6'>札幌市中央区南5条西6丁目第3エイトビル4F
                  <br />
                  ※道案内: 地下鉄南北線すすきのより約5分/札幌市電資生館小学校前駅より約3分</span>
              </div>
              <div className='boder-info mb-10'></div>
              <div className='d-flex mb-10'>
                <span className='d-flex flex-1'>電話番号</span>
                <span className='text-info d-flex'>011-213-0873</span>
              </div>
              <div className='boder-info mb-10'></div>
              <div className='d-flex mb-10'>
                <span className='d-flex flex-1'>営業時間</span>
                <span className='text-info d-flex'>20:00 〜 6:00 :00</span>
              </div>
              <div className='boder-info mb-10'></div>
              <div className='d-flex mb-10'>
                <span className='d-flex flex-1'>定休日</span>
                <span className='text-info d-flex'>日曜日（祝前日が日曜日の場合は営業。振替で月曜日がお休みとなります）</span>
              </div>
              <div className='boder-info mb-10'></div>
              <div className='d-flex mb-10'>
                <span className='d-flex flex-1'>客席</span>
                <span className='text-info d-flex'>60席</span>
              </div>
              <div className='boder-info mb-10'></div>
              <div className='d-flex mb-10'>
                <span className='d-flex flex-1'>喫煙</span>
                <span className='text-info d-flex mb-5'>喫煙可</span>
              </div>
              <div className='boder-info mb-10'></div>
              <div className='d-flex mb-10'>
                <span className='d-flex flex-1'>カード</span>
                <span className='text-info d-flex mb-5'>VISA､マスター､アメックス､DINERS､JCB､Discover</span>
              </div>
              <div className='boder-info mb-10'></div>
            </div>
            <div className='image-info w-2/5'>
              <img className='item-1' src={item3} alt="food.png" />
            </div>
          </div>
        </div>
        <div>
          <div className="title-page w-100 d-flex center middle p-14 m-0">
            <span className='text-[36px] text-[#DDC298] font-bold '>
              店舗情報
              <div className="border-title"></div>
            </span>
          </div>
          <div className='map'>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d3353.662578218937!2d130.7076814!3d32.801205!3m2!1i1024!2i768!4f13.1!2m1!1z54aK5pys55yM54aK5pys5biC5Lit5aSu5Yy65LiL6YCaMS02LTfjgqLjgqTjgqLjgqTjg5Pjg6tCMUY!5e0!3m2!1sja!2sjp!4v1610193783259!5m2!1sja!2sjp"
              width="100%" height="400" style={{ border: 0 }} aria-hidden="false"
            ></iframe>
          </div>
        </div>
        <div className="footer-homepage">
          <div className="d-flex middle center">
            <span className="text-2xl mb-10 mt-4">Follow us</span>
          </div>
          <div className="icon d-flex middle center p-5 pb-15">
            <div className='media d-flex justify-center center mr-10'>
              <FaInstagram className='cursor-pointer' size={23} />
            </div>
            <div className='media d-flex justify-center center'>
              <FiTwitter className='cursor-pointer' size={23} />
            </div>
          </div>
          <div className="top-to-btm">
            {' '}
            {showTopBtn && <FaAngleUp className="icon-position icon-style cursor-pointer" onClick={goToTop} />}{' '}
          </div>
          <div className="border-footer mb-1"></div>
        </div>
      </div>
    </>) ||
      currentTab === 1 && (
        <>
          <TakeOut setCurrentTab={setCurrentTab} 
           />
        </>
      ) || currentTab === 2 && (
        <>
          <TableBooking setCurrentTab={setCurrentTab} />
        </>
      )
    }
  </div>
)
}

export default HomePage;

