import React, { useState } from 'react';
import { Route, Switch, Link, useLocation } from 'react-router-dom';
import './assets/scss/index.scss';
import { MasterRouter } from 'features/master';
import HomePage from 'features/home';
import TableBooking from 'components/TableBooking/TableBooking';
import TakeOut from 'components/TakeOut/TakeOut';
import { Breadcrumb } from 'antd';
import Item from 'antd/lib/list/Item';

function App() {
  return (
    <>
      <Switch>
        <MasterRouter path="/" component={() => <HomePage />} />
        {/* <Route path="/" element={<HomePage />} /> */}
        <Route path="/tablebooking" component={TableBooking} />
        <Route path="/takeout" component={TakeOut} />
      </Switch>
    </>
  );
}

export default App;
