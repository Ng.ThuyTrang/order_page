import React from "react";

interface InputType {
  label?: string,
  error?: string,
  className?: string,
  value?: string | number,
  type?: string,
  onChange?: React.ChangeEventHandler<HTMLInputElement> | undefined
  onKeyPress?: React.KeyboardEventHandler<HTMLInputElement> | undefined,
  placeholder?: string | undefined
}

const TextInput = (input: InputType) => {
  const { label, error, ...props } = input;
  return (
    <div className={`input-login ${error ? "error" : ""}`}>
      <h5>{label}</h5>
      <input {...props} />
    </div>
  );
}


export default TextInput;
