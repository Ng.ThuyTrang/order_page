module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
   
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: "#2E6B8B",
        overlay: "rgba(0,0,0,.5)",
      },
      inset: {
        26: "6.5rem",
        "-26": "-6.5rem",
      },
      spacing: {
        68: "16.875rem",
      },
      fontFamily: {
        main: ['"NotoSansJP"'],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
